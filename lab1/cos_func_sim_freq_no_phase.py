
import numpy as np
import scipy
#import scipy.spatial
from scipy.io import wavfile
#import pylab
import matplotlib.pyplot as plt

(sample_rate_eh, input_signal_eh) = wavfile.read("audio_files/vowel_eh.wav")
(sample_rate_ah, input_signal_ah) = wavfile.read("audio_files/vowel_ah.wav")

time_array_ah = np.arange(0, len(input_signal_ah)/sample_rate_ah, 1/sample_rate_ah)
time_array_ah = time_array_ah[0:(sample_rate_ah//10)]

input_signal_ah = input_signal_ah[0:(sample_rate_eh//10)]
f_input_signal_ah=np.asfarray(input_signal_ah)
f_input_signal_ah = f_input_signal_ah/ np.linalg.norm(f_input_signal_ah)

guessed_sin_cycle = 9.0
guessed_ang_freq=(guessed_sin_cycle*2*np.pi)/0.1

guessed_cos_signal = np.cos(guessed_ang_freq*(time_array_ah))
guessed_cos_signal = guessed_cos_signal* np.amax(f_input_signal_ah)

f, axarr = plt.subplots(2, sharex=True)
f.set_size_inches(12,30)
axarr[0].set_title('a red signal with a blue cos function with similar frequency and no phase')
axarr[0].plot(time_array_ah,f_input_signal_ah,color='r')
axarr[0].plot(time_array_ah,guessed_cos_signal,color='b')



pointwise_products = guessed_cos_signal * f_input_signal_ah
pointwise_products =np.sqrt(pointwise_products**2)
axarr[1].plot(time_array_ah,pointwise_products,color='c')
axarr[1].set_title('absolute values of the pointwise products of above')
plt.show()