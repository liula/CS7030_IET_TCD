import numpy as np
import scipy
import scipy.spatial
from scipy.io import wavfile
#import pylab
import matplotlib.pyplot as plt

(sample_rate_eh, input_signal_eh) = wavfile.read("audio_files/vowel_eh.wav")
(sample_rate_ah, input_signal_ah) = wavfile.read("audio_files/vowel_ah.wav")

time_array_ah = np.arange(0, len(input_signal_ah)/sample_rate_ah, 1/sample_rate_ah)
time_array_ah = time_array_ah[0:(sample_rate_ah//10)]

input_signal_ah = input_signal_ah[0:(sample_rate_eh//10)]
f_input_signal_ah=np.asfarray(input_signal_ah)
f_input_signal_ah = f_input_signal_ah/ np.linalg.norm(f_input_signal_ah)

guessed_cos_cycle = 9.0
guessed_ang_freq=(guessed_cos_cycle*2*np.pi)/0.1


guessed_cos_signal = np.cos(guessed_ang_freq*(time_array_ah))
guessed_cos_signal = guessed_cos_signal* np.amax(f_input_signal_ah)

guessed_sin_signal = np.sin(guessed_ang_freq*(time_array_ah))
guessed_sin_signal = guessed_sin_signal* np.amax(f_input_signal_ah)


suitable_phase= 1.7*np.pi

good_match_sin= np.sin(guessed_ang_freq*(time_array_ah)+suitable_phase)
good_match_sin=good_match_sin*np.amax(f_input_signal_ah)
two_plots_sum= guessed_cos_signal+guessed_sin_signal


#signal_dif= two_plots_sum*good_match_sin
#print (Magnitude of product of the two signals  in second plots is %0.3E' %np.linalg.norm(signal_dif))



f, axarr = plt.subplots(2, sharex=True)
f.set_size_inches(12,30)
axarr[0].set_title('a red signal with a blue sin function which is a good match')
axarr[0].plot(time_array_ah,f_input_signal_ah,color='r')
axarr[0].plot(time_array_ah,good_match_sin,color='b')



axarr[1].set_title('sum of two previous plots(red) and the good match(blue) ')
axarr[1].plot(time_array_ah,two_plots_sum,color='r')
axarr[1].plot(time_array_ah,good_match_sin,color='b')


cos_sim_sum_match= np.dot(two_plots_sum,good_match_sin)/(np.linalg.norm(two_plots_sum)*np.linalg.norm(good_match_sin))
cos_sim_match=np.dot(f_input_signal_ah,good_match_sin)/(np.linalg.norm(f_input_signal_ah)*np.linalg.norm(good_match_sin))
cos_sim_sum=np.dot(two_plots_sum,f_input_signal_ah)/(np.linalg.norm(two_plots_sum)*np.linalg.norm(f_input_signal_ah))


print('Magnitude of sum of two previous plots with similar frequencies is %0.3E'%np.linalg.norm(two_plots_sum))
print('Magnitude of good match sin plot is %0.3E'%np.linalg.norm(good_match_sin))
print('Euclidean distance between the sum and the good match is {:0.3f}'.format( scipy.spatial.distance.euclidean(two_plots_sum,good_match_sin)))

print('Cosine Similarity between the sum and the input signal is %0.3E'%cos_sim_sum)
print('Cosine Similarity between the good match and the input signal is %0.3E'%cos_sim_match)
print('Cosine Similarity between the sum and the good match is %0.3E'%cos_sim_sum_match)


#different = two_plots_sum-good_match_sin


plt.show()